#Read audio files
import os, fnmatch
import glob
import numpy as np
import librosa
import random
from pathlib import Path
from sklearn.preprocessing import normalize
import matplotlib.pyplot as plt
from scipy import signal


############################################################
############## Generate Babble Noise #######################
############################################################
def make_same_length(track, size): #can probably be replaced with np.pad
    if size<len(track): #If the size is smaller, we want to clip
        output = track[:size]
    else:
        output = np.zeros(size)
        output[:len(track)] = track
    return output
#Overlay audio files
def overlay_tracks(files, normalize = True, clip_on_first = False):
    if len(files) < 2:
        raise Exception("overlay error. Need more than 1 file.")
    tracks = []
    sampling_rates = []
    #Load audio files
    for file in files:
        if type(file)==str:
            y, sr = librosa.load(file)
        else:
            y = file[0]
            sr = file[1]
        tracks.append(np.array(y))
        sampling_rates.append(sr)
    #Mix
    output = tracks[0]
    for ind in range(1,len(tracks)):
        track = tracks[ind]
        if(sampling_rates[ind]!=sampling_rates[0]):
            raise Exception("overlay error. Sampling rates must be equal.")
        #Make sure they're the same length
        len_output = len(output)
        len_track = len(track)
        if clip_on_first:
            track = make_same_length(track, len_output)
        elif len_output > len_track:
            track = make_same_length(track, len_output)
        elif len_track > len_output:
            output = make_same_length(output, len_track)
        #Standardize to remove negatives
        # output = output + np.min(output)
        # track = track +np.min(track)
        output = np.sum([output, track],axis=0)
        # output = output + track
        # output = output - np.median(output) #Shift back down

    if normalize:
        return np.true_divide(output ,len(tracks)),sampling_rates[0]
    else:
        return output, sampling_rates[0]

#Get babble WAV files
babble_files_path = '.\Babble_Noisers'
babble_files = glob.iglob(babble_files_path+'\*.WAV', recursive=False)
babble_files = list(babble_files)
print("babble files:",len(babble_files))

#Calls overlay_tracks and saves the output wave file.
def generate_babble_noise(babble_files,num_tracks_to_use,output_filename):
    if len(babble_files)<2:
        raise Exception("Error generating babble noise. Not enough babble files.")
    inds = random.sample(range(0,len(babble_files)),k=num_tracks_to_use)
    print("Generating babble noise using files ",inds)
    files = []
    for ind in inds:
        files.append(babble_files[ind])
    mixed_track, sr = overlay_tracks(files)
    librosa.output.write_wav(output_filename, mixed_track, sr)
    return mixed_track, sr

# #Generate filenames
# num_babble_noises_to_generate = 4  #How many files to generate
# num_babble_files_to_use = 3 #How many files to overlay
# generated_babble_noise_filenames = [] #Saves the generated filenames
# for bn in range(0,num_babble_noises_to_generate):
#     generated_babble_noise_filename = './babble_noise_'+str(bn)+'.wav'
#     generated_babble_noise_filenames.append(generated_babble_noise_filename)
#     generate_babble_noise(babble_files,num_babble_files_to_use,generated_babble_noise_filename) #Start overlaying.
#     print("Generated babble noise file ",bn+1)

#####################################################################################
############## Overlay Babble Noise w/ Speech at Specific dBA #######################
#####################################################################################

#Recursively walk through a directory. References https://stackoverflow.com/a/2186673
def find_files(directory, pattern):
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.join(root, basename)
                yield filename

#Get speech WAV files recursively into each speaker's folder
speech_files_path = '.\Subjects_Actual_Used_Data'
speech_files = find_files(speech_files_path, '*.WAV')
speech_files = list(speech_files)
print("speech files:",len(speech_files))

#Iterate through speaker files and overlay babble noise at a specific SNR
#Referencing http://www.cromulentrambling.com/2014/11/adding-noise-of-certain-snr-to-audio.html

def get_mean(track):
    return np.mean(track)


def get_max(track):
    return np.max(track)



def get_mean_periodogram(track,fs):
    f, Pxx_spec = signal.periodogram(track, fs, 'flattop', scaling='spectrum')
    # return np.mean(np.sqrt(Pxx_spec))
    # return np.sqrt(Pxx_spec.max())
    return np.sqrt(Pxx_spec.mean())
    # return (Pxx_spec.max())

def get_mean_power(track,sr):
    return np.mean(np.abs(np.fft.fft(track))**2)

def get_power(track):
    return np.sum((np.diff(track))**2)

def snr(P1, P2):
    return 10 * np.log10(P1/P2)
def snr_amplitude(A1,A2):
    return (A1/A2)**2
def amplitude_to_db(amplitude):
    dB = -1*20 * np.log10(amplitude)
    return dB
def db_to_amplitude(dB):
    amplitude = 10**(-1*dB/20)
    return amplitude

def amplify_track(track, current_db, desired_db):
    return track * np.sqrt(current_db/(10**(desired_db/10)))
    # return track * desired_db/current_db
def amplify_track_amplitude(track, current_amp, desired_amp):
    ratio = (current_amp)/ (desired_amp)
    print("Amplifying ratio",  ratio)
    return track * ratio

noisy_filename_suffix = "_noisy"
babble_filename_suffix = "_generated_babble_noise"
num_babble_files_to_use = 3 #How many babble files to overlay
for speech_file in speech_files:
    dir = os.path.dirname(speech_file)
    filename, file_extension = os.path.splitext(os.path.basename(speech_file))
    #Generate unique babble noise
    generated_babble_noise_filename = dir+'\\'+filename+babble_filename_suffix+file_extension
    babble_track, babble_sr = generate_babble_noise(babble_files,num_babble_files_to_use,generated_babble_noise_filename) #Start overlaying
    # babble_track = babble_track.reshape((1,-1))
    #Read speech file:
    speech_track, speech_sr = librosa.load(speech_file)
    print ("shape",speech_track.shape)
    # speech_track = speech_track.reshape((1,-1))
    #Computer Powers
    speech_power = get_mean_power(speech_track, speech_sr)
    print("initial speech power", (speech_power))
    babble_power = get_mean_power(babble_track, babble_sr)
    #Amplify to make them have the same powers:
    desired_speech_power = 65
    speech_track = amplify_track(speech_track,(speech_power),(desired_speech_power))
    speech_power = get_mean_power(speech_track,speech_sr)
    print("amplified speech power in db", (speech_power))
    babble_track = amplify_track(babble_track, babble_power,speech_power)
    babble_power = get_mean_power(babble_track, babble_sr)
    print("amplified babble power in db", (babble_power))


    #Save output
    amplified_speech_filename = dir+'\\'+filename+'_amplified_speech_'+file_extension
    librosa.output.write_wav(amplified_speech_filename, speech_track, speech_sr)


    SNR = snr_amplitude((speech_power),(babble_power))
    # SNR = 10 * np.log10(speech_power/babble_power)
    print("Default SNR: ", SNR)
    #Overlay at specific SNR:
    desired_SNR = (-0.4)
    # K = np.sqrt(speech_power/(10**(desired_SNR/10)))
    # K = np.sqrt(speech_power/(10**(db/10)))
    # K = speech_power/(10**(desired_SNR/10))
    # K = (speech_power)*np.sqrt(np.abs(desired_SNR))
    # K = ((speech_power)-36)
    # K = speech_power/np.sqrt((desired_SNR))-speech_power
    # K = (speech_power) / (10**(desired_SNR/10))
    # K = np.sqrt((speech_power)/(10**(desired_SNR/10)))
    K = (speech_power) / (10**(desired_SNR/10))
    print("Computed K is ", K)
    K = (K)/100

    babble_track = amplify_track(babble_track,babble_power,K)
    babble_power = get_mean_power(babble_track, babble_sr)
    print("reduced babble in db",(babble_power))
    #Save output
    reduced_babble_filename = dir+'\\'+filename+'_reduced_babble'+file_extension
    librosa.output.write_wav(reduced_babble_filename, babble_track, babble_sr)


    mixed_track, sr = overlay_tracks([(speech_track,speech_sr), (babble_track,babble_sr)], True,True) #Overlay without normalizing

    mixed_power = get_mean_power(mixed_track,sr)
    print("mixed power", (mixed_power))

    #Amplify signal. This didn't yield good results. Sound track too low volume, ironically.
    # desired_mixed_power = 95
    # mixed_track = amplify_track(mixed_track,mixed_power,(desired_mixed_power))
    # mixed_power = get_mean_power(mixed_track,sr)
    # print("mixed power after amplification", (mixed_power))
    print("speech power",(speech_power))
    print("babble power",(babble_power))
    print("New SNR with noise", snr((speech_power), (babble_power)))
    print("New SNR with mixed", snr((speech_power), (mixed_power)))
    #Save output
    noisy_speech_filename = dir+'\\'+filename+noisy_filename_suffix+file_extension
    librosa.output.write_wav(noisy_speech_filename, mixed_track, sr)
    print("Done overlaying ", filename, "with babble noise")


print("Done")
