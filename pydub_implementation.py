#Read audio files
import os, fnmatch
import glob
import numpy as np
import random
from pathlib import Path
from pydub import AudioSegment #References https://github.com/jiaaro/pydub/blob/master/API.markdown

#Get babble WAV files
babble_files_path = '.\Babble_Noisers'
babble_files = glob.iglob(babble_files_path+'\*.WAV', recursive=False)
babble_files = list(babble_files)
print("babble files:",len(babble_files))

#Calls overlay_tracks and saves the output wave file.
def generate_babble_noise(babble_files,num_tracks_to_use,output_filename):
    if len(babble_files)<2:
        raise Exception("Error generating babble noise. Not enough babble files.")
    inds = random.sample(range(0,len(babble_files)),k=num_tracks_to_use)
    print("Generating babble noise using files ",inds)
    output = AudioSegment.from_file(babble_files[inds[0]])
    for ind in range(1,len(inds)):
        track = AudioSegment.from_file(babble_files[inds[ind]])
        output = output.overlay(track, loop=True)
    output.export(output_filename, format="wav")
    return output


#####################################################################################
############## Overlay Babble Noise w/ Speech at Specific dBA #######################
#####################################################################################

#Recursively walk through a directory. References https://stackoverflow.com/a/2186673
def find_files(directory, pattern):
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.join(root, basename)
                yield filename

#Get speech WAV files recursively into each speaker's folder
speech_files_path = '.\Subjects_Actual_Used_Data'
speech_files = find_files(speech_files_path, '*.WAV')
speech_files = list(speech_files)
print("speech files:",len(speech_files))

def snr(P1, P2):
    return P1-P2
def printdBFS(sound, name):
    print("Power for ",name, sound.dBFS)
noisy_filename_suffix = "_noisy"
babble_filename_suffix = "_generated_babble_noise"
num_babble_files_to_use = 3 #How many babble files to overlay
for speech_file in speech_files:
    dir = os.path.dirname(speech_file)
    filename, file_extension = os.path.splitext(os.path.basename(speech_file))
    #Generate unique babble noise
    generated_babble_noise_filename = dir+'\\'+filename+babble_filename_suffix+file_extension
    babble_track = generate_babble_noise(babble_files,num_babble_files_to_use,generated_babble_noise_filename) #Start overlaying
    #Read speech file
    speech_track = AudioSegment.from_file(speech_file)
    #Computer Powers
    printdBFS(speech_track, "Speech Track")
    printdBFS(babble_track, "Babble Track")
    #Normalize them, referencing https://github.com/netankit/AudioMLProject1/blob/master/speech_noise_ir_audio_mixing_script.py
    # speech_track = pydub.effects.normalize(speech_track)
    speech_track = speech_track.append(AudioSegment.silent(duration=1000,frame_rate = speech_track.frame_rate))
    speech_track = speech_track.apply_gain(-speech_track.max_dBFS)
    speech_track_normalized_filename = dir+'\\'+filename+'_speech_normalized_'+file_extension
    speech_track.export(speech_track_normalized_filename)
    # babble_track = pydub.effects.normalize(babble_track)
    babble_track = babble_track.apply_gain(-babble_track.max_dBFS)
    printdBFS(speech_track, "Speech Track Normalized")
    printdBFS(babble_track, "Babble Track Normalized")
    #SNR = signal - noise, noise = snr - signal:
    desired_SNRs = [-9.3, -4.8, -2.0, 0, 2.7]
    for desired_SNR in desired_SNRs:
        print("Desired SNR",desired_SNR)
        noise = desired_SNR - speech_track.dBFS
        babble_track_snr = babble_track - (babble_track.dBFS+noise)
        print("Calculated SNR gain needed",noise)
        print("Calculated babble after SNR gain needed",babble_track_snr.dBFS)
        speech_snr_track = speech_track.overlay(babble_track_snr, loop=True)
        #Computer Powers
        printdBFS(speech_snr_track, "Speech mixed with babble")
        print("SNR with speech and babble snr", (snr(speech_track.dBFS,babble_track_snr.dBFS)))
        print("SNR with speech power and mixed", (snr(speech_track.dBFS,speech_snr_track.dBFS)))

        snr_speech_filename = dir+'\\'+filename+'_snr'+str(desired_SNR)+'_speech_'+file_extension
        # speech_snr_track = speech_snr_track.apply_gain(-speech_snr_track.max_dBFS)
        speech_snr_track.export(snr_speech_filename)

    cost = 0
    cost_increment = 0.01
    tolerance = 0.01
    last_sign = 0
    speech_snr_track = []
    for desired_SNR in desired_SNRs:
        calculated_snr = tolerance+desired_SNR+1
        print("Desired SNR",desired_SNR, "Calculated SNR",calculated_snr)
        max_iterations = 1000
        counter = 0
        while (not (calculated_snr < desired_SNR + tolerance and calculated_snr>desired_SNR-tolerance)):
            counter = counter +1
            if counter == max_iterations:
                break
            noise = desired_SNR - speech_track.dBFS + cost
            babble_track_snr = babble_track - (babble_track.dBFS+noise)
            print("Calculated SNR gain needed",noise)
            print("Calculated babble after SNR gain needed",babble_track_snr.dBFS)
            speech_snr_track = speech_track.overlay(babble_track_snr, loop=True)
            #Computer Powers
            printdBFS(speech_snr_track, "Speech mixed with babble")
            print("SNR with speech and babble snr", (snr(speech_track.dBFS,babble_track_snr.dBFS)))
            print("SNR with speech power and mixed", (snr(speech_track.dBFS,speech_snr_track.dBFS)))

            calculated_snr = snr(speech_track.dBFS,speech_snr_track.dBFS)
            if calculated_snr>desired_SNR+cost:
                sign = -1
                if last_sign == 0:
                    last_sign = sign
                if last_sign != sign:
                    cost_increment = cost_increment/10
                cost = cost - cost_increment
            else:
                sign = -1
                if last_sign == 0:
                    last_sign = sign
                if last_sign != sign:
                    cost_increment = cost_increment/10
                cost = cost + cost_increment
        snr_speech_filename = dir+'\\'+filename+'_mixing_snr'+str(desired_SNR)+'_speech_'+file_extension
        # speech_snr_track = speech_snr_track.apply_gain(-speech_snr_track.max_dBFS)
        speech_snr_track.export(snr_speech_filename)
    print("Done overlaying ", filename, "with babble noise")


print("Done")

